# Build stage
FROM python:3.10.1-slim

LABEL maintainer="Nevrona FOSS <foss@nevrona.org>"
LABEL org.opencontainers.image.title="Poetry Docker Image"
LABEL org.opencontainers.image.description="Docker image for Poetry"
LABEL org.opencontainers.image.vendor="Nevrona S. A."
LABEL org.opencontainers.image.authors="HacKan <hackan@nevrona.org>"
LABEL org.opencontainers.image.version="0.4.0"
LABEL org.opencontainers.image.ref.name="0.4.0"
LABEL org.opencontainers.image.licenses="MPL-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/nevrona/public/poetry-docker"
LABEL org.opencontainers.image.url="https://gitlab.com/nevrona/public/poetry-docker"
LABEL org.opencontainers.image.created=""
LABEL org.opencontainers.image.revision=""
LABEL org.nevrona.url="https://nevrona.org"

ARG PYTHONUNBUFFERED=1
ARG POETRY_VIRTUALENVS_CREATE=0
ARG POETRY_VERSION

RUN apt-get update \
    && apt-get upgrade -qy --no-install-recommends \
    && apt-get install -qy --no-install-recommends \
        build-essential=12.9 \
        libffi-dev=3.3-6 \
        git=1:2.30.2-1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apt/*

WORKDIR /tmp/poetry
COPY install-poetry.py .
RUN python install-poetry.py --yes \
    && rm -rf /tmp/poetry

ENV PATH="/root/.local/bin:$PATH"

WORKDIR /
