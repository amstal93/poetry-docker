# Poetry Docker Image

A Poetry Docker image that ensures reproducibility and hash checking for Poetry and its dependencies, to be used to build other images.

## Usage

Use this image to build another image, where you require access to Poetry (usually in a stage in multistage images):

`FROM registry.gitlab.com/nevrona/public/poetry-docker:<version>`

## Versions

This project will publish versions equal to Poetry versions, so it's easier to use. So for Poetry v1.2.3, use `registry.gitlab.com/nevrona/public/poetry-docker:1.2.3`.

**Important**: since v0.3 the base image is no longer Alpine but Debian (slim).

## License

*Poetry docker* is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

**Neither the name of Nevrona S. A. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.**
